<div width="100%" align="center">
  <h1>Wax Editor</h1>
</div>

| [![MIT license](https://img.shields.io/badge/license-MIT-e51879.svg)](https://gitlab.coko.foundation/wax/wax-prosemirror/raw/master/LICENSE) |
| :------------------------------------------------------------------------------------------------------------------------------------------: |


Wax Editor is built on top of the Prosemirror library. Check Prosemirror [website](https://prosemirror.net/) and [GitHub repo](https://github.com/ProseMirror) for more information.

## [Various Demos.](https://demo.waxjs.net/)


## Get up and running

Run a local version of the demos

1.  `yarn with node >= 14`

2.  `yarn build`

3.  `yarn start` Will bring up a demos of different ediitors

## Documentation

Documentation can be found [here](https://waxjs.net/docs/wax/).
